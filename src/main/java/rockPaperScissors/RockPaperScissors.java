package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true){
            Random rand =new Random();
            String randomWord=rpsChoices.get(rand.nextInt((rpsChoices.size()))); //Velger et tilfeldig ord fra liste rpschoices
            System.out.println("Let's play round "+roundCounter);
            String userInput=readInput("Your choice (Rock/Paper/Scissors)?");
            if (!rpsChoices.contains(userInput)){
                System.out.println("I do not understand "+userInput+". Could you try again?");
                userInput=readInput("Your choice (Rock/Paper/Scissors)?");
            }

            String choice_string="Human chose "+userInput+", computer chose "+randomWord;
            if (isWinner(userInput,randomWord)){ //if the method returns true, when we put userchoice as choice 1 then the human wins
                System.out.println(choice_string+". Human wins.");
                humanScore+=1;
            }
            else if(isWinner(randomWord, userInput)){ //if the method returns true, when the computers choice is first then the computer wins
                System.out.println(choice_string+". Computer wins.");
                computerScore+=1;
            }
            else//And if both returns false it is tie
                System.out.println(choice_string+ ". It's a tie.");
            
            System.out.println("Score: human "+humanScore+", computer "+computerScore);

            String userChoice=readInput("Do you wish to continue playing? (y/n)?");
            if (userChoice.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }

            roundCounter+=1;
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next().toLowerCase().trim();
        return userInput;
    }
    public static boolean isWinner(String choice1,String choice2){
        if (choice1.equals("paper")){
            return choice2.equals("rock");
        }
        else if (choice1.equals("scissors")){
            return choice2.equals("paper");
        }
        else;
            return choice2.equals("scissors");
    }

}
